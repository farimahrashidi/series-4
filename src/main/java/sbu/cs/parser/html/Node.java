package sbu.cs.parser.html;

import java.util.List;
import sbu.cs.parser.json.JsonMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
public class Node implements NodeInterface {
    private Map<String , String> KeyValues = new HashMap<>();
    private  List<Node> tags = new ArrayList<>();
    private String orgTag;

    public Node(String orgTag) {
        this.orgTag = orgTag;
    }
    public void setTags(Node tag) {
        tags.add(tag);
    }
    public String getTags() {
        return orgTag;
    }
    /*private String data;
    public Node(String s){
        this.data = s;*/
    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        // TODO implement this
        if(!closedOrNott(orgTag)){
            return null;
        }
        String firstTagName =  "<" +nameOfTag(orgTag)+ ">";
        String secondTagName = "</" +nameOfTag(orgTag)+ ">";
        String str = orgTag.substring(orgTag.indexOf(firstTagName)+firstTagName.length() , orgTag.indexOf(secondTagName));
        return str;
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {
        int size = tags.size();
        for(int j=0 ; j < size ; j++){
            Node newTag = HTMLParser.parse(tags.get(j).orgTag);
            tags.set(j , newTag);
        }
        return tags;
    }

    public String nameOfTag(String orgTag){
        return orgTag.substring(1 + orgTag.indexOf("<") , orgTag.indexOf(">"));
    }

    public boolean closedOrNott(String orgTag){
        if(1 + orgTag.indexOf("<") == orgTag.indexOf("/")){
            return false;
        }
        else{
            return true;
        }
    }
    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        // TODO implement this
        String[] KeyValue;
        String str = orgTag.substring(1 + orgTag.indexOf(" "), orgTag.indexOf(">"));
        str = str.replaceAll("\" " , "#").replaceAll("//s" , "=");
        KeyValue = str.split("#");
        for(int j=0 ; j<KeyValue.length ; j++){
            String[] mp = KeyValue[j].split("=");
            KeyValues.put(mp[0].replaceAll("\"" , "") , mp[1].replaceAll("\"" , ""));
        }
        return KeyValues.get(key);
    }
}
