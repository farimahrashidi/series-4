package sbu.cs.parser.html;
import java.util.List;
import java.util.ArrayList;
public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        // TODO implement this
        Node obj = new Node(document);
        document = document.replaceAll("\n" , "");
        document = checkTagName(document);
        while(document.contains("<")){
            obj.setTags(checkTag(document));
            document = removeTag(document);
        }
        return obj;
    }
    public static boolean isAttributeInside(String document){
        int start = document.indexOf("<");
        int end = document.indexOf(">");
        String str = document.substring(start + 1 , end);
        if(str.contains("=")){
            return true;
        }
        else{
            return false;
        }
    }
    public static String checkTagName(String document){
        if(isAttributeInside(document) == true){
            return removeName1(document);
        }
        else{
            return removeName2(document);
        }
    }
    public static String removeName1(String document){
        int index1 = document.indexOf("<");
        int index2 = document.indexOf(" ");
        String firstTagName = "<" + getTagName(document) + ">";
        String secondTagName = "</" + document.substring(index1 + 1 , index2) + ">";
        String str = document.replace(firstTagName , "").replaceAll(secondTagName , "");
        return str;
    }
    public static String removeName2(String document){
        String firstTagName = "<" + getTagName(document) + ">";
        String secondTagName = "</" + getTagName(document) + ">";
        String str = document.replace(firstTagName , "").replaceAll(secondTagName , "");
        return str;
    }
    public static String getTagName(String document){
        int startName = document.indexOf("<");
        int endName = document.indexOf(">");
        String name = document.substring(startName + 1 , endName);
        return name;
    }
    public static Node checkTag(String document){
        if(closedOrNot(document) == false){
            return returnTagHasEnd(document);
        }
        else{
            return returnTagHasNotEnd(document);
        }
    }
    public static Node returnTagHasEnd(String document){
        String str = document.substring(document.indexOf("<") , document.indexOf(">")+1);
        Node obj = new Node(str);
        return obj;
    }
    public static Node returnTagHasNotEnd(String document){
        if(isAttributeInside(document)){
            String firstTagName = "<" +getTagName(document)+ ">";
            String str = document.substring(document.indexOf("<")+1 , document.indexOf(" "));
            String secondTagName = "</" + str + ">";
            String tagg = document.substring(document.indexOf(firstTagName) , document.indexOf(secondTagName)+secondTagName.length());
            Node obj = new Node(tagg);
            return obj;
        }
        else{
            String firstTagName = "<" +getTagName(document)+ ">";
            String secondTagName = "</" +getTagName(document)+ ">";
            String str = document.substring(document.indexOf(firstTagName) , document.indexOf(secondTagName)+secondTagName.length());
            Node obj = new Node(str);
            return obj;
        }
    }

    public static boolean closedOrNot(String document){
        if(1 + document.indexOf("<") == document.indexOf("/")){
            return false;
        }
        else{
            return true;
        }
    }
    public static String removeTag(String document){
        //injaro check konm:
        String doc = checkTag(document).getTags();
        return document.replaceAll(doc , "");
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
