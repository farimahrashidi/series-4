package sbu.cs.parser.json;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        // TODO implement this
        data = data.replaceAll("[ \\s\"{}]","");
        String copy = data;

        int x = data.indexOf('[');
        int y = data.indexOf(']');

        while(copy.indexOf('[') >= 0){
            String str = copy.substring(x,y+1);
            data = data.replace(str , str.replaceAll(",","&"));
            copy = copy.replace(str,"");

        }
        /////////
        /*for(int j=x ; j<y ; j++)
        {
            if(data.charAt(j) == ','){

            }
        }*/
        String[] d = data.split(",");

        ArrayList<JsonMap> keyvalue = new ArrayList<>();

        for(int i=0 ; i<d.length ; i++)
        {
            String[] newStr = d[i].split(":");
            keyvalue.add(new JsonMap(newStr[0],newStr[1]));
        }
        Json maps = new Json(keyvalue);
        return maps;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
