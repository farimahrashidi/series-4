package sbu.cs.parser.json;

public class Json implements JsonInterface {
    private ArrayList<JsonMap> ListOfKeyvalue = new ArrayList<>();

    public Json(ArrayList ListOfKeyvalue){
        this.ListOfKeyvalue = ListOfKeyvalue;
    }

    @Override
    public String getStringValue(String key) {
        // TODO implement this
        for(int i=0 ; i<ListOfKeyvalue.size() ; i++){
            if(key.equals(ListOfKeyvalue.get(i).getKey())){
                return ListOfKeyvalue.get(i).getValue();
            }
        }

        return null;
    }
}
