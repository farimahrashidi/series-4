package sbu.cs.parser.json;

public class JsonMap {
    private String key;
    private String value;

    public JsonMap(String key,String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        if(value.indexOf('[') >=0){
            value = value.replaceAll("&",", ");
        }
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
